package com.example.demo.controller;

import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Report;
import com.example.demo.service.CommentService;
import com.example.demo.service.ReportService;

@Controller
public class ForumController {
	@Autowired
	ReportService reportService;
	@Autowired
	CommentService commentService;

	// 投稿内容表示画面
	@GetMapping
	public ModelAndView top(@ModelAttribute("start_date") String start, @ModelAttribute("end_date") String end) throws ParseException {
		ModelAndView mav = new ModelAndView();
		List<Report> contentData = reportService.findByCreatedDataBetween(start, end); // 投稿を全件取得
		List<Comment> commentData = commentService.findByCreatedDataBetween(start, end);
		mav.setViewName("/top"); // 画面遷移先を指定
		mav.addObject("start", start);
		mav.addObject("end", end);
		mav.addObject("contents", contentData); // 投稿データオブジェクトを保管
		mav.addObject("comments", commentData); // 返信データオブジェクトを保管

		return mav;
	}

	// 新規投稿画面
	@GetMapping("/new")
	public ModelAndView newContent() {
		ModelAndView mav = new ModelAndView();
		Report report = new Report(); // form用の空のentityを準備
		mav.setViewName("/new"); // 画面遷移先を指定
		mav.addObject("formModel", report); // 準備した空のentityを保管
		return mav;
	}

	// 投稿処理
	@PostMapping("/add")
	public ModelAndView addContent(@ModelAttribute("formModel") Report report) {
		reportService.saveReport(report); // 投稿をテーブルに格納
		return new ModelAndView("redirect:/"); // rootへリダイレクト
	}

	// 削除処理
	@DeleteMapping("/delete/{id}")
	public ModelAndView deleteContent(@PathVariable Integer id) {
		reportService.deleteReport(id); // UrlParameterのidを基に投稿を削除
		return new ModelAndView("redirect:/"); // rootへリダイレクト
	}

	// 編集画面
	@GetMapping("/edit/{id}")
	public ModelAndView editContent(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		Report report = reportService.editReport(id); // 編集する投稿を取得
		mav.addObject("formModel", report); // 編集する投稿をセット
		mav.setViewName("/edit"); // 画面遷移先を指定
		return mav;
	}

	// 編集処理
	@PutMapping("/update/{id}")
	public ModelAndView updateContent(@PathVariable Integer id, @ModelAttribute("formModel") Report report) {
		report.setId(id); // UrlParameterのidを更新するentityにセット
		reportService.saveReport(report); // 編集した投稿を更新
		return new ModelAndView("redirect:/"); // rootへリダイレクト
	}

	// 返信投稿処理
	@PostMapping("/addComment/{report_id}")
	public ModelAndView addComment(@PathVariable Integer report_id, @ModelAttribute("formModel") Comment comment) {
		comment.setReportId(report_id); // 投稿をテーブルに格納
		commentService.saveComment(comment); // 編集した投稿を更新
		return new ModelAndView("redirect:/"); // rootへリダイレクト
	}

	// 返信編集画面
	@GetMapping("/commentEdit/{id}")
	public ModelAndView editComment(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		Comment comment = commentService.editComment(id); // 編集する投稿を取得
		mav.addObject("formModel", comment); // 編集する投稿をセット
		mav.setViewName("/commentEdit"); // 画面遷移先を指定
		return mav;
	}

	// 返信編集処理
	@PutMapping("/commentUpdate/{id}")
	public ModelAndView updateComment(@PathVariable Integer id, @ModelAttribute("formModel") Comment comment) {
		Comment commentReportId = commentService.editComment(id);
		comment.setReportId(commentReportId.getReportId()); // UrlParameterのidを更新するentityにセット
		commentService.updateComment(comment); // 編集した投稿を更新
		return new ModelAndView("redirect:/"); // rootへリダイレクト
	}

	// 削除処理
	@DeleteMapping("/commentDelete/{id}")
	public ModelAndView deleteComment(@PathVariable Integer id) {
		commentService.deleteComment(id); // UrlParameterのidを基に投稿を削除
		return new ModelAndView("redirect:/"); // rootへリダイレクト
	}
}
