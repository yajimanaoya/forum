package com.example.demo.service;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Comment;
import com.example.demo.repository.CommentRepository;

@Service
public class CommentService {
	@Autowired
	CommentRepository commentRepository;

	final String START_DATE_INITIAL_VALUE = "2020-01-01 00:00:00";

	// レコード全件取得
		public List<Comment> findByCreatedDataBetween(String start, String end) throws ParseException {
			Date date = new Date();
	        long time = date.getTime();
	        Timestamp ts = new Timestamp(time);

			String startDate;
	        String endDate;
	        if ((start != null) && (!start.isEmpty())) {
	        	startDate = start + " 00:00:00";
	        } else {
	        	startDate = START_DATE_INITIAL_VALUE;
	        }
	        if ((end != null) && (!end.isEmpty())) {
	        	endDate = end + " 23:59:59";
	        } else {
	        	endDate = ts.toString();
	        }
	        SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	        Date startDateTime = sdFormat.parse(startDate);
	        Date endDateTime = sdFormat.parse(endDate);
			return commentRepository.findByCreatedDataBetweenOrderByCreatedDataDesc(startDateTime, endDateTime);
		}

	// レコード追加
	public void saveComment(Comment comment) {
		commentRepository.save(comment);
	}

	// レコード1件取得
	public Comment editComment(Integer id) {
		Comment comment = (Comment) commentRepository.findById(id).orElse(null);
		return comment;
	}

	// 返信更新
	public void updateComment(Comment comment) {
		commentRepository.save(comment);
	}

	// 削除処理
	public void deleteComment(Integer id) {
		commentRepository.deleteById(id);
	}

}
